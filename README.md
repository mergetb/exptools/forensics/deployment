# Experiments

Experiment models and automation playbooks

#### tl;dr
Jump to [#Run your experiment"](#run-your-experiment) for a working end-to-end example

## Define your experiment

Each subdirectory in [models/](models) typically defines 3 things:
- a Merge experiment (MX) model file
- an ansible inventory file(s) that describe each node's roles
- an ansible playbook that bootstraps the nodes

Some notes:
- The ansible playbook should perform the necessary steps so that the topology can transit traffic. 
- The ansible playbook __should not__ generate traffic or collect measurements. Those tasks are done in the [deployment/](deployment) apparatus
- The ansible inventory __should__ use well defined groups to describe the nodes so that the deployment apparatus can control it. See table below

### Node inventory
| Group | Meaning |
| :-    | :-      |
| all | All nodes |
| databases | Control nodes where measurement data will be stored |
| end_hosts | Nodes that will send or receive application traffic |
| routers | Nodes that route layer 3 traffic |
| bridges | Nodes that forward layer 2 traffic |

Additional groups can be created and used in the experiment's ansible playbook as needed.

## Define your traffic

Experiments can also define a traffic inventory that descibe how traffic should be generated. Traffic inventory groups are specific to each type of generator (currently, this is just iperf) See [models/bridge/traffic/10g.yml](models/bridge/traffic/10g.yml) as an example:
```yml
clients:
  vars:
    tags:
      id: 10g-udp-test
  hosts:
    l0: 
      port: 5201
      host: 192.168.100.200
      bandwidth: 10g 
      proto: udp
servers:
  hosts:
    r0: 
      port: 5201
```

This describes a simple iperf experiment where
- `r0` is a server and listens on port `5201`
- `l0` is a client and connects to host `192.168.100.200:5201`. It uses UDP and sends 10g of data per second.
- The iperf performance data collected from the `client` is tagged with `id:10g-udp-test` in the influxdb database for the experiment

Each client or server in the inventory should include options to customize the
traffic as needed. See
[https://gitlab.com/mergetb/exptools/forensics/iperf](https://gitlab.com/mergetb/exptools/forensics/iperf)
for a description of the available options

## Run your experiment

1. Create, realize, and materialize the experiment on a Merge testbed
    - see, e.g., [models/bridge/model.py](models/bridge/model.py)
1. Create an XDC and attach it to the materialization
    ```shell
    mrg new xdc x0.murphy
    mrg xdc attach x0.murphy <mzid>
    ```
1. Bootstrap the XDC (only needed once per XDC)
    ```
    sudo bash xdc/init-xdc-defaults.sh
    ```
1. Run the ansible playbook:
    ```
    cd deployment
    ansible-playbook \
        -i <experiment inventory> \
        [-i <additional inventories> ...] \
        -e experiment_playbook="<experiment ansible playbook>" \
        run.yml
    ```

    For example,
    ```
    ansible-playbook \
        -i ../models/bridge/model.ini \
        -i ../models/bridge/traffic/10g.yml \
        -e experiment_playbook="../models/bridge/bootstrap.yml" \
        run.yml
    ```


## Analyze data

Nodes marked as `database` nodes run an InfluxDB instance. Conventionally, we add a special control node to each experiment model called `overwatch` for this purpose.

Once you've run an experiment as above, you can access the results in the database. For example, to calculate the average gbps and packet loss rate from the above:
```bash
$ influx -host overwatch -database forensics
Connected to http://overwatch:8086 version 1.8.10
InfluxDB shell version: 1.6.4
> select mean(bits_per_second)/1000000000,sum(lost_packets)*100/sum(packets) from iperf3 where id='10g-udp-test'
name: iperf3
time mean               sum_sum
---- ----               -------
0    1.7427933601669319 22.776999916413924
> 
```

We sent 1.74 Gbps and experienced ~23% packet loss
