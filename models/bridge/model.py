from mergexp import *

# Create a network topology object.
net = Network('fourbyfour')

ft = net.node('ft', image=="1804", memory.capacity>=gb(32), proc.cores>=2)

num_nodes=4

for side in [ 'l', 'r' ]:
    nodes = [ ]
    hundred = 100 if side == 'l' else 200
    for host in range(num_nodes):
        hostname = side + str(host)
        nodes.append(net.node(hostname, image=="1804"))
    link = net.connect([ft] + nodes)
    for host in range(num_nodes):
        addr = '192.168.100.%d/24' % (hundred + host)
        link[nodes[host]].socket.addrs = ip4(addr)

ov = net.node('overwatch', image=="2004", memory.capacity>=gb(8), proc.cores>=2)

experiment(net)
