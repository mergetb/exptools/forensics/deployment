from mergexp import *

def merge_node(name, bare=False, img="2004", groups=[]):
    n = net.node(name, metal==bare, image==img, memory.capacity == gb(8), proc.cores == 4)

    if len(groups) > 0:
        n.properties["group"] = tuple(groups)

    return n

def enforcer(name):
    return merge_node(name, img="1804", groups=["dpdk_bridges"])

def router(name):
    return merge_node(name, groups=["routers"])

def switch(name):
    return merge_node(name, groups=["ovs_switches"])

def end_host(name):
    return merge_node(name, groups=["end_hosts"])

class Enclave(object):
    def __init__(self, net, idx):
        self.idx = idx

        # nodes
        self.hosts    = [end_host("h%d" % (idx*2 + i)) for i in range(2)]
        self.router   = router("b%d" % idx)
        self.switch   = switch("sw%d" % idx)
        self.enforcer = enforcer("e%d" % idx)

        # connections
        for h in self.hosts:
            net.connect([h, self.router]) 

        # connections to switch are layer2
        net.connect([self.switch, self.router], layer==2, capacity==mbps(50))

        # 2 connections to the enforcer
        net.connect([self.switch, self.enforcer], layer==2)
        net.connect([self.switch, self.enforcer], layer==2)

    def address(self):
        for idx, h in enumerate(self.hosts):
            hidx = self.idx*2 + idx 
            major = int(hidx / 2)
            minor = int(hidx % 2)
            h.sockets[0].addrs = ip4('10.%d.%d.100/24' % (major, minor))

        self.router.sockets[0].addrs = ip4('10.%d.0.1/24' % self.idx)
        self.router.sockets[1].addrs = ip4('10.%d.1.1/24' % self.idx)
        self.router.sockets[2].addrs = ip4('10.%d.254.2/24' % self.idx)

class Core(object):
    def __init__(self, net, idx, enclaves):
        self.idx = idx

        self.node     = router("c%d" % self.idx)
        self.enclaves = enclaves
        self.parent   = None
        self.child    = None

        for e in enclaves:
            net.connect([self.node, e.switch], layer==2)

    def connect(self, net, parent):
        self.parent = parent
        parent.child = self
        net.connect([self.node, parent.node], capacity==mbps(50))

    def address(self):
        # addresses on border-facing NICs
        for idx, e in enumerate(self.enclaves):
            self.node.sockets[idx].addrs = ip4('10.%d.254.1/24' % e.idx)

        nic = len(self.enclaves)

        # parent/child facing NICs
        if self.child is not None:
            self.node.sockets[nic].addrs = ip4('10.254.%d.1/24' % self.child.idx)
            nic += 1

        if self.parent is not None:
            self.node.sockets[nic].addrs = ip4('10.254.%d.2/24' % self.idx)

# create a topology
net = Network("dqm", routing == static)

enclaves = [Enclave(net, i) for i in range(2)]
core = Core(net, 0, enclaves)

for e in enclaves:
    e.address()

core.address()

# start the experiment
experiment(net)
